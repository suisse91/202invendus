#!/usr/bin/env ruby
# coding: utf-8

class Invendus
  def initialize a, b
    @a = a.to_f
    @b = b.to_f
    @resTab = Array.new
    @prices = [10.0, 20.0, 30.0, 40.0, 50.0]
    @z = Hash.new(0)
    6.times { |a| @resTab[a] = Array.new(0) }
  end

  def loiConjointe x, y
    res1 = (@a - x) * (@b - y)
    res2 = (5.0 * @a - 150.0) * (5.0 * @b - 150.0)
    res1 / res2
  end

  def calcLoiConjointe
    @prices.each_with_index { |a, y|
      @prices.each_with_index { |b, x|
        @resTab[x][y] = loiConjointe(a, b)
      }
    }
  end

  def probaZ
    @prices.each { |x|
      @prices.each { |y|
        @z[x + y] += loiConjointe(x, y)
      }
    }
  end

  def loiMarginaleX
    5.times { |idx|
      @resTab[5][idx] = 0
      5.times { |i|
        @resTab[5][idx] += @resTab[i][idx]
      }
    }
    @resTab[5][5] = 1
  end

  def loiMarginaleY
    5.times { |idx|
      @resTab[idx][5] = 0
      5.times { |i|
        @resTab[idx][5] += @resTab[idx][i]
      }
    }
  end

  def variance x, y
    x**2 * y
  end

  def esperanceX
    res = 0
    5.times { |i|
      res += (@prices[i] * @resTab[5][i])
    }
    res
  end

  def esperanceY
    res = 0
    5.times { |i|
      res += (@prices[i] * @resTab[i][5])
    }
    res
  end

  def esperanceZ
    res = 0
    @z.keys.each { |k|
      res += (k * @z[k])
    }
    res
  end

  def varianceY
    res = 0
    5.times { |i|
      res += variance(@prices[i], @resTab[i][5])
    }
    res -= esperanceY**2
    puts "Espérance de Y: %.3f" % esperanceY
    puts "Variance de Y: %.3f" % res
  end

  def varianceX
    res = 0
    5.times { |i|
      res += variance(@prices[i], @resTab[5][i])
    }
    res -= esperanceX**2
    puts "Espérance de X: %.3f" % esperanceX
    puts "Variance de X: %.3f" % res
  end

  def varianceZ
    res = 0
    @z.keys.each_with_index { |k, idx|
      res += (k**2 * @z[k])
    }
    res -= esperanceZ**2;
    puts "Espérance de Z: %.3f" % esperanceZ
    puts "Variance de Z: %.3f" % res
  end

  
  def affTable
    # Tableau X, Y
    print "          "
    @prices.each { |a| print "X=#{a.to_i}        "}
    puts "loi de Y"
    @prices.each_with_index { |a, idx|
      print "Y=#{a.to_i}      "
      @resTab[idx].each { |val|
        print "%.3f       " % val
      }
      puts
    }
    # Loi de X
    print "loi de X  "
    @resTab[5].each { |a| print "%.3f       " % a }
    puts
    # Proba de Z
    print "z       "
    @z.keys.each { |k| print "#{k.to_i}     "}
    puts "  total"
    print "p(Z=z)  "
    @z.values.each { |v| print "%.3f  " % v}
    puts "   1"
  end
end
